# Proj0-Hello
-------------
**Author:** Ryan Gurnick
**Email Address:** [rgurnick@uoregon.edu](rgurnick@uoregon.edu)

-------------
# Application Description

The purpose of this application is to ensure that we have created the credentials.ini file correctly and that the python `hello.py` application is able to access that file. This will allow us to check that github is setup correctly in essence testing that we have not committed that file directly. Other than that we pretty much just print the message from the credentials configuration file.

Here is my updated README, I hope you enjoy!

-------------
# Use

```
# make run
(cd hello; python3 hello.py)
Hello World
```
